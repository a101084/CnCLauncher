@echo off
title=正在更新
echo 正在更新中，请稍后...
ping 127.0.0.1 -n 1 >nul
ren CnCLauncher.exe CnCLauncher.old.exe
7z.exe x -y NewCnCLauncher.zip
del 7z.exe
del 7z.dll
del NewCnCLauncher.zip
if exist CnCLauncher.exe (
	start CnCLauncher.exe
	del %0
) else (
	cls
	title=更新失败
	echo 更新失败，请到项目主页按照说明重新下载安装本程序。
	echo https://gitee.com/a101084/CnCLauncher/
	start explorer.exe "https://gitee.com/a101084/CnCLauncher/"
	pause >NUL
)
