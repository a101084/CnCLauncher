﻿Module PubModule
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Int32, ByVal lpFileName As String) As Int32
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Int32

    Public intVer As Integer = 1585976400 '2020-04-04 13:00:00
    Public strURI As String = "https://gitee.com/a101084/CnCLauncher/raw/master/"
    'Public strURI As String = "file:///F:/TEMP/Blog/gitee.com/CnCLauncher/"
    Public strUpdateURI As String = "https://gitee.com/a101084/CnCLauncher/"
    Public strConfig = Left(Application.ExecutablePath, Len(Application.ExecutablePath) - 4) + ".ini"

    Public Function GetINI(ByVal Section As String, ByVal AppName As String, ByVal lpDefault As String, ByVal FileName As String) As String
        Dim Str As String = New String(" ", 512)
        GetPrivateProfileString(Section, AppName, lpDefault, Str, Len(Str), FileName)
        Return Left(Str, InStr(Str, Chr(0)) - 1)
    End Function

    Public Function WriteINI(ByVal Section As String, ByVal AppName As String, ByVal lpDefault As String, ByVal FileName As String) As Long
        WriteINI = WritePrivateProfileString(Section, AppName, lpDefault, FileName)
    End Function

    Public Function GetMD5(ByVal strSource As String) As String
        Dim result As String = ""
        Try
            Dim fstream As New IO.FileStream(strSource, IO.FileMode.Open, IO.FileAccess.Read)
            Dim dataToHash(fstream.Length - 1) As Byte
            fstream.Read(dataToHash, 0, fstream.Length)
            fstream.Close()
            Dim hashvalue As Byte() = CType(Security.Cryptography.CryptoConfig.CreateFromName("MD5"), Security.Cryptography.HashAlgorithm).ComputeHash(dataToHash)
            Dim i As Integer
            For i = 0 To hashvalue.Length - 1
                result += Right("00" + Hex(hashvalue(i)).ToLower, 2)
            Next
            Return result
        Catch ex As Exception
            'MsgBox(ex.Message)
            Return result
        End Try
    End Function
End Module
