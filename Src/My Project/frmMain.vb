﻿Imports System.Threading

Public Class FrmMain
    Declare Function LoadCursorFromFile Lib "user32" Alias "LoadCursorFromFileA" (ByVal lpFileName As String) As IntPtr

    Dim flgLock1 As Boolean
    Dim flgLock2 As Boolean
    Dim flgPage As Integer '存放菜单页面编号，默认从0开始
    Dim MousePos As Integer

    Dim arrPath(12) As String

    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckForIllegalCrossThreadCalls = False '不进行跨线程检查

        Me.SetClientSizeCore(800, 600)
        Me.Left = Screen.PrimaryScreen.Bounds.Width / 2 - Me.Width / 2
        Me.Top = Screen.PrimaryScreen.Bounds.Height / 2 - Me.Height / 2

        PicLogo.Left = ClientSize.Width / 2 - PicLogo.Width / 2
        PicLogo.Top = 10
        LblExit.Left = ClientSize.Width - LblExit.Width - 10
        LblExit.Top = ClientSize.Height - LblExit.Height - 10
        LblSetting.Left = ClientSize.Width - LblSetting.Width - 10
        LblSetting.Top = ClientSize.Height - LblSetting.Height - LblExit.Height - 10

        PicCC1.Parent = PalList
        PicRA1.Parent = PalList
        PicCC2.Parent = PalList
        PicRenegade.Parent = PalList
        PicRA2.Parent = PalList
        PicGenerals.Parent = PalList
        PicCC3.Parent = PalList
        PicRA3.Parent = PalList
        PicCC4.Parent = PalList

        LoadConfig()
        LoadMenu()

        '必须在可视为真时设置背景图，否则菜单不显示
        PicCC3.Visible = False
        PicRA3.Visible = False
        PicCC4.Visible = False
    End Sub

    Private Sub FrmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Dim CheckThread = New Thread(AddressOf CheckUpdate)
        CheckThread.Start()
    End Sub

    Private Sub CheckUpdate()
        Dim strVerLog As String
        Dim strTmp() As String
        Dim Web As New Net.WebClient
        Dim strLogURI As New Uri(PubModule.strURI & "VerLog.txt")
        Try
            strVerLog = Web.DownloadString(strLogURI)
        Catch ex As Exception
            strVerLog = "0,5pqC5peg5pu05paw5pel5b+X,00000000000000000000000000000000"
        End Try
        strTmp = Split(strVerLog, ",")
        Dim strLog As String
        strLog = System.Text.Encoding.GetEncoding("utf-8").GetString(Convert.FromBase64String(strTmp(1)))
        If (CInt(strTmp(0)) > intVer) Then
            'MessageBox.Show(Me, "发现新的版本，请到下载页面手动更新。" + vbCrLf + vbCrLf + "更新日志：" + vbCrLf + strLog, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Process.Start(strUpdateURI)
            Dim msg As DialogResult
            msg = MessageBox.Show(Me, "发现新的版本，请点击确定下载更新。" + vbCrLf + vbCrLf + "更新日志：" + vbCrLf + strLog, "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If (msg = DialogResult.OK) Then
                Dim dFile As New Net.WebClient
                Me.Visible = False
                dFile.DownloadFile(New Uri(PubModule.strURI & "NewCnCLauncher.zip"), "NewCnCLauncher.zip")
                If (GetMD5("NewCnCLauncher.zip") = LCase(strTmp(2))) Then
                    IO.File.WriteAllText("renew.bat", My.Resources.Resources.renew, encoding:=System.Text.Encoding.Default)
                    IO.File.WriteAllBytes("7z.exe", My.Resources.Resources._7z)
                    IO.File.WriteAllBytes("7z.dll", My.Resources.Resources._7zdll)
                    Dim pInfo As New ProcessStartInfo()
                    pInfo.FileName = "renew.bat"
                    pInfo.WorkingDirectory = Application.StartupPath
                    Process.Start(pInfo)
                Else
                    MessageBox.Show(Me, "更新失败，请稍后重试。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            End If
            Close()
        End If
    End Sub

    Private Sub LoadConfig()
        arrPath(0) = GetINI("CnCLauncher", "CC1", "", strConfig)
        arrPath(1) = GetINI("CnCLauncher", "RA1", "", strConfig)
        arrPath(2) = GetINI("CnCLauncher", "CC2", "", strConfig)
        arrPath(3) = GetINI("CnCLauncher", "Renegade", "", strConfig)
        arrPath(4) = GetINI("CnCLauncher", "RA2", "", strConfig)
        arrPath(5) = GetINI("CnCLauncher", "RA2YURI", "", strConfig)
        arrPath(6) = GetINI("CnCLauncher", "Generals", "", strConfig)
        arrPath(7) = GetINI("CnCLauncher", "GeneralsZH", "", strConfig)
        arrPath(8) = GetINI("CnCLauncher", "CC3", "", strConfig)
        arrPath(9) = GetINI("CnCLauncher", "CC3KW", "", strConfig)
        arrPath(10) = GetINI("CnCLauncher", "RA3", "", strConfig)
        arrPath(11) = GetINI("CnCLauncher", "RA3Uprising", "", strConfig)
        arrPath(12) = GetINI("CnCLauncher", "CC4", "", strConfig)

        Dim i As Integer
        For i = 0 To 12
            arrPath(i) = IIf(IO.File.Exists(arrPath(i)), arrPath(i), "")
        Next

        Dim BGPath As String
        BGPath = GetINI("Setting", "bg", "", strConfig)
        If (BGPath <> "" And IO.File.Exists(BGPath)) Then Me.BackgroundImage = Image.FromFile(BGPath)

        Dim CURPath As String
        CURPath = GetINI("Setting", "CUR", "", strConfig)
        If Not (CURPath <> "" And IO.File.Exists(CURPath)) Then
            IO.File.WriteAllBytes("Pointer.ani", My.Resources.Resources.Pointer)
            CURPath = Application.StartupPath + "\Pointer.ani"
        End If
        Dim myCursor As New Cursor(Cursor.Current.Handle)
        Dim colorCursorHandle As IntPtr = LoadCursorFromFile(CURPath)
        myCursor.GetType().InvokeMember("handle", Reflection.BindingFlags.Public Or Reflection.BindingFlags.NonPublic Or Reflection.BindingFlags.Instance Or Reflection.BindingFlags.SetField, Nothing, myCursor, New Object() {colorCursorHandle})
        Me.Cursor = myCursor
    End Sub

    Private Sub LoadMenu()
        If (arrPath(0) = "") Then
            PicCC1.BackgroundImage = My.Resources.Resources.caption0103
            PicCC1.Enabled = False
        Else
            PicCC1.BackgroundImage = My.Resources.Resources.caption0101
        End If
        If (arrPath(1) = "") Then
            PicRA1.BackgroundImage = My.Resources.Resources.caption0203
            PicRA1.Enabled = False
        Else
            PicRA1.BackgroundImage = My.Resources.Resources.caption0201
        End If
        If (arrPath(2) = "") Then
            PicCC2.BackgroundImage = My.Resources.Resources.caption0303
            PicCC2.Enabled = False
        Else
            PicCC2.BackgroundImage = My.Resources.Resources.caption0301
        End If
        If (arrPath(3) = "") Then
            PicRenegade.BackgroundImage = My.Resources.Resources.caption0403
            PicRenegade.Enabled = False
        Else
            PicRenegade.BackgroundImage = My.Resources.Resources.caption0401
        End If
        If (arrPath(4) = "") Then
            PicRA2.BackgroundImage = My.Resources.Resources.caption0503
        Else
            PicRA2.BackgroundImage = My.Resources.Resources.caption0501
        End If
        If (arrPath(5) = "") Then
            PicRA2.Image = My.Resources.Resources.caption0603
        Else
            PicRA2.Image = My.Resources.Resources.caption0601
        End If
        If (arrPath(6) = "") Then
            PicGenerals.BackgroundImage = My.Resources.Resources.caption0703
        Else
            PicGenerals.BackgroundImage = My.Resources.Resources.caption0701
        End If
        If (arrPath(7) = "") Then
            PicGenerals.Image = My.Resources.Resources.caption0803
        Else
            PicGenerals.Image = My.Resources.Resources.caption0801
        End If
        If (arrPath(8) = "") Then
            PicCC3.BackgroundImage = My.Resources.Resources.caption0903
        Else
            PicCC3.BackgroundImage = My.Resources.Resources.caption0901
        End If
        If (arrPath(9) = "") Then
            PicCC3.Image = My.Resources.Resources.caption1003
        Else
            PicCC3.Image = My.Resources.Resources.caption1001
        End If
        If (arrPath(10) = "") Then
            PicRA3.BackgroundImage = My.Resources.Resources.caption1103
        Else
            PicRA3.BackgroundImage = My.Resources.Resources.caption1101
        End If
        If (arrPath(11) = "") Then
            PicRA3.Image = My.Resources.Resources.caption1203
        Else
            PicRA3.Image = My.Resources.Resources.caption1201
        End If
        If (arrPath(12) = "") Then
            PicCC4.BackgroundImage = My.Resources.Resources.caption1303
            PicCC4.Enabled = False
        Else
            PicCC4.BackgroundImage = My.Resources.Resources.caption1301
        End If
    End Sub

    Private Sub LblSetting_Click(sender As Object, e As EventArgs) Handles LblSetting.Click
        frmSetting.ShowDialog()
    End Sub

    Private Sub LblSetting_MouseMove(sender As Object, e As MouseEventArgs) Handles LblSetting.MouseMove
        Dim LabelFont As Font = New Font(LblSetting.Font.Name, LblSetting.Font.Size, FontStyle.Underline Or FontStyle.Bold)
        LblSetting.Font.Dispose()
        LblSetting.Font = LabelFont
    End Sub

    Private Sub LblSetting_MouseLeave(sender As Object, e As EventArgs) Handles LblSetting.MouseLeave
        Dim LabelFont As Font = New Font(LblSetting.Font.Name, LblSetting.Font.Size, FontStyle.Bold)
        LblSetting.Font.Dispose()
        LblSetting.Font = LabelFont
    End Sub

    Private Sub LblExit_Click(sender As Object, e As EventArgs) Handles LblExit.Click
        Close()
    End Sub

    Private Sub LblExit_MouseMove(sender As Object, e As MouseEventArgs) Handles LblExit.MouseMove
        Dim LabelFont As Font = New Font(LblExit.Font.Name, LblExit.Font.Size, FontStyle.Underline Or FontStyle.Bold)
        LblExit.Font.Dispose()
        LblExit.Font = LabelFont
    End Sub

    Private Sub LblExit_MouseLeave(sender As Object, e As EventArgs) Handles LblExit.MouseLeave
        Dim LabelFont As Font = New Font(LblExit.Font.Name, LblExit.Font.Size, FontStyle.Bold)
        LblExit.Font.Dispose()
        LblExit.Font = LabelFont
    End Sub

    Private Sub PicCC1_Click(sender As Object, e As EventArgs) Handles PicCC1.Click
        My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
        Dim pInfo As New ProcessStartInfo()
        pInfo.FileName = arrPath(0)
        pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(0))
        Dim p As Process = Process.Start(pInfo)
        Me.Visible = False
        p.WaitForExit()
        Me.Visible = True
    End Sub

    Private Sub PicCC1_MouseMove(sender As Object, e As MouseEventArgs) Handles PicCC1.MouseMove
        If (flgLock1 = False) Then
            My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
            flgLock1 = True
            PicCC1.BackgroundImage = My.Resources.Resources.caption0102
        End If
    End Sub

    Private Sub PicCC1_MouseLeave(sender As Object, e As EventArgs) Handles PicCC1.MouseLeave
        flgLock1 = False
        PicCC1.BackgroundImage = My.Resources.Resources.caption0101
    End Sub

    Private Sub PicRA1_Click(sender As Object, e As EventArgs) Handles PicRA1.Click
        My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
        Dim pInfo As New ProcessStartInfo()
        pInfo.FileName = arrPath(1)
        pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(1))
        Dim p As Process = Process.Start(pInfo)
        Me.Visible = False
        p.WaitForExit()
        Me.Visible = True
    End Sub

    Private Sub PicRA1_MouseMove(sender As Object, e As MouseEventArgs) Handles PicRA1.MouseMove
        If (flgLock1 = False) Then
            My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
            flgLock1 = True
            PicRA1.BackgroundImage = My.Resources.Resources.caption0202
        End If
    End Sub

    Private Sub PicRA1_MouseLeave(sender As Object, e As EventArgs) Handles PicRA1.MouseLeave
        flgLock1 = False
        PicRA1.BackgroundImage = My.Resources.Resources.caption0201
    End Sub

    Private Sub PicCC2_Click(sender As Object, e As EventArgs) Handles PicCC2.Click
        My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
        Dim pInfo As New ProcessStartInfo()
        pInfo.FileName = arrPath(2)
        pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(2))
        Dim p As Process = Process.Start(pInfo)
        Me.Visible = False
        p.WaitForExit()
        Me.Visible = True
    End Sub

    Private Sub PicCC2_MouseMove(sender As Object, e As MouseEventArgs) Handles PicCC2.MouseMove
        If (flgLock1 = False) Then
            My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
            flgLock1 = True
            PicCC2.BackgroundImage = My.Resources.Resources.caption0302
        End If
    End Sub

    Private Sub PicCC2_MouseLeave(sender As Object, e As EventArgs) Handles PicCC2.MouseLeave
        flgLock1 = False
        PicCC2.BackgroundImage = My.Resources.Resources.caption0301
    End Sub

    Private Sub PicRenegade_Click(sender As Object, e As EventArgs) Handles PicRenegade.Click
        My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
        Dim pInfo As New ProcessStartInfo()
        pInfo.FileName = arrPath(3)
        pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(3))
        Dim p As Process = Process.Start(pInfo)
        Me.Visible = False
        p.WaitForExit()
        Me.Visible = True
    End Sub

    Private Sub PicRenegade_MouseMove(sender As Object, e As MouseEventArgs) Handles PicRenegade.MouseMove
        If (flgLock1 = False) Then
            My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
            flgLock1 = True
            PicRenegade.BackgroundImage = My.Resources.Resources.caption0402
        End If
    End Sub

    Private Sub PicRenegade_MouseLeave(sender As Object, e As EventArgs) Handles PicRenegade.MouseLeave
        flgLock1 = False
        PicRenegade.BackgroundImage = My.Resources.Resources.caption0401
    End Sub

    Private Sub PicRA2_Click(sender As Object, e As EventArgs) Handles PicRA2.Click
        If (MousePos < 35) Then
            If (arrPath(4) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(4)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(4))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        Else
            If (arrPath(5) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(5)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(5))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        End If
    End Sub

    Private Sub PicRA2_MouseMove(sender As Object, e As MouseEventArgs) Handles PicRA2.MouseMove
        MousePos = e.Y
        If (MousePos < 35) Then
            If (flgLock1 = False) Then
                flgLock1 = True
                flgLock2 = False
                If (arrPath(5) = "") Then
                    PicRA2.Image = My.Resources.Resources.caption0603
                Else
                    PicRA2.Image = My.Resources.Resources.caption0601
                End If
                If (arrPath(4) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicRA2.BackgroundImage = My.Resources.Resources.caption0502
            End If
        Else
            If (flgLock2 = False) Then
                flgLock1 = False
                flgLock2 = True
                If (arrPath(4) = "") Then
                    PicRA2.BackgroundImage = My.Resources.Resources.caption0503
                Else
                    PicRA2.BackgroundImage = My.Resources.Resources.caption0501
                End If
                If (arrPath(5) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicRA2.Image = My.Resources.Resources.caption0602
            End If
        End If
    End Sub

    Private Sub PicRA2_MouseLeave(sender As Object, e As EventArgs) Handles PicRA2.MouseLeave
        flgLock1 = False
        flgLock2 = False
        If (arrPath(4) = "") Then
            PicRA2.BackgroundImage = My.Resources.Resources.caption0503
        Else
            PicRA2.BackgroundImage = My.Resources.Resources.caption0501
        End If
        If (arrPath(5) = "") Then
            PicRA2.Image = My.Resources.Resources.caption0603
        Else
            PicRA2.Image = My.Resources.Resources.caption0601
        End If
    End Sub

    Private Sub PicGenerals_Click(sender As Object, e As EventArgs) Handles PicGenerals.Click
        If (MousePos < 35) Then
            If (arrPath(6) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(6)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(6))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        Else
            If (arrPath(7) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(7)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(7))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        End If
    End Sub

    Private Sub PicGenerals_MouseMove(sender As Object, e As MouseEventArgs) Handles PicGenerals.MouseMove
        MousePos = e.Y
        If (MousePos < 35) Then
            If (flgLock1 = False) Then
                flgLock1 = True
                flgLock2 = False
                If (arrPath(7) = "") Then
                    PicGenerals.Image = My.Resources.Resources.caption0803
                Else
                    PicGenerals.Image = My.Resources.Resources.caption0801
                End If
                If (arrPath(6) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicGenerals.BackgroundImage = My.Resources.Resources.caption0702
            End If
        Else
            If (flgLock2 = False) Then
                flgLock1 = False
                flgLock2 = True
                If (arrPath(6) = "") Then
                    PicGenerals.BackgroundImage = My.Resources.Resources.caption0703
                Else
                    PicGenerals.BackgroundImage = My.Resources.Resources.caption0701
                End If
                If (arrPath(7) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicGenerals.Image = My.Resources.Resources.caption0802
            End If
        End If
    End Sub

    Private Sub PicGenerals_MouseLeave(sender As Object, e As EventArgs) Handles PicGenerals.MouseLeave
        flgLock1 = False
        flgLock2 = False
        If (arrPath(6) = "") Then
            PicGenerals.BackgroundImage = My.Resources.Resources.caption0703
        Else
            PicGenerals.BackgroundImage = My.Resources.Resources.caption0701
        End If
        If (arrPath(7) = "") Then
            PicGenerals.Image = My.Resources.Resources.caption0803
        Else
            PicGenerals.Image = My.Resources.Resources.caption0801
        End If
    End Sub

    Private Sub PicCC3_Click(sender As Object, e As EventArgs) Handles PicCC3.Click
        If (MousePos < 35) Then
            If (arrPath(8) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(8)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(8))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        Else
            If (arrPath(9) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(9)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(9))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        End If
    End Sub

    Private Sub PicCC3_MouseMove(sender As Object, e As MouseEventArgs) Handles PicCC3.MouseMove
        MousePos = e.Y
        If (MousePos < 35) Then
            If (flgLock1 = False) Then
                flgLock1 = True
                flgLock2 = False
                If (arrPath(9) = "") Then
                    PicCC3.Image = My.Resources.Resources.caption1003
                Else
                    PicCC3.Image = My.Resources.Resources.caption1001
                End If
                If (arrPath(8) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicCC3.BackgroundImage = My.Resources.Resources.caption0902
            End If
        Else
            If (flgLock2 = False) Then
                flgLock1 = False
                flgLock2 = True
                If (arrPath(8) = "") Then
                    PicCC3.BackgroundImage = My.Resources.Resources.caption0903
                Else
                    PicCC3.BackgroundImage = My.Resources.Resources.caption0901
                End If
                If (arrPath(9) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicCC3.Image = My.Resources.Resources.caption1002
            End If
        End If
    End Sub

    Private Sub PicCC3_MouseLeave(sender As Object, e As EventArgs) Handles PicCC3.MouseLeave
        flgLock1 = False
        flgLock2 = False
        If (arrPath(8) = "") Then
            PicCC3.BackgroundImage = My.Resources.Resources.caption0903
        Else
            PicCC3.BackgroundImage = My.Resources.Resources.caption0901
        End If
        If (arrPath(9) = "") Then
            PicCC3.Image = My.Resources.Resources.caption1003
        Else
            PicCC3.Image = My.Resources.Resources.caption1001
        End If
    End Sub

    Private Sub PicRA3_Click(sender As Object, e As EventArgs) Handles PicRA3.Click
        If (MousePos < 35) Then
            If (arrPath(10) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(10)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(10))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        Else
            If (arrPath(11) = "") Then Exit Sub
            My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
            Dim pInfo As New ProcessStartInfo()
            pInfo.FileName = arrPath(11)
            pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(11))
            Dim p As Process = Process.Start(pInfo)
            Me.Visible = False
            p.WaitForExit()
            Me.Visible = True
        End If
    End Sub

    Private Sub PicRA3_MouseMove(sender As Object, e As MouseEventArgs) Handles PicRA3.MouseMove
        MousePos = e.Y
        If (MousePos < 35) Then
            If (flgLock1 = False) Then
                flgLock1 = True
                flgLock2 = False
                If (arrPath(11) = "") Then
                    PicRA3.Image = My.Resources.Resources.caption1203
                Else
                    PicRA3.Image = My.Resources.Resources.caption1201
                End If
                If (arrPath(10) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicRA3.BackgroundImage = My.Resources.Resources.caption1102
            End If
        Else
            If (flgLock2 = False) Then
                flgLock1 = False
                flgLock2 = True
                If (arrPath(10) = "") Then
                    PicRA3.BackgroundImage = My.Resources.Resources.caption1103
                Else
                    PicRA3.BackgroundImage = My.Resources.Resources.caption1101
                End If
                If (arrPath(11) = "") Then Exit Sub
                My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
                PicRA3.Image = My.Resources.Resources.caption1202
            End If
        End If
    End Sub

    Private Sub PicRA3_MouseLeave(sender As Object, e As EventArgs) Handles PicRA3.MouseLeave
        flgLock1 = False
        flgLock2 = False
        If (arrPath(10) = "") Then
            PicRA3.BackgroundImage = My.Resources.Resources.caption1103
        Else
            PicRA3.BackgroundImage = My.Resources.Resources.caption1101
        End If
        If (arrPath(11) = "") Then
            PicRA3.Image = My.Resources.Resources.caption1203
        Else
            PicRA3.Image = My.Resources.Resources.caption1201
        End If
    End Sub

    Private Sub PicCC4_Click(sender As Object, e As EventArgs) Handles PicCC4.Click
        My.Computer.Audio.Play(My.Resources.Resources.MouseClick, AudioPlayMode.Background)
        Dim pInfo As New ProcessStartInfo()
        pInfo.FileName = arrPath(12)
        pInfo.WorkingDirectory = IO.Path.GetDirectoryName(arrPath(12))
        Dim p As Process = Process.Start(pInfo)
        Me.Visible = False
        p.WaitForExit()
        Me.Visible = True
    End Sub

    Private Sub PicCC4_MouseMove(sender As Object, e As MouseEventArgs) Handles PicCC4.MouseMove
        If (flgLock1 = False) Then
            My.Computer.Audio.Play(My.Resources.Resources.MouseMove, AudioPlayMode.Background)
            flgLock1 = True
            PicCC4.BackgroundImage = My.Resources.Resources.caption1302
        End If
    End Sub

    Private Sub PicCC4_MouseLeave(sender As Object, e As EventArgs) Handles PicCC4.MouseLeave
        flgLock1 = False
        PicCC4.BackgroundImage = My.Resources.Resources.caption1301
    End Sub

    Private Sub PicArrow_Click(sender As Object, e As EventArgs) Handles PicArrow.Click
        Dim oThread As Thread

        Select Case (flgPage)
            Case 0
                oThread = New Thread(AddressOf AniThread1)
                oThread.Start()
                flgPage = 1
                PicArrow.BackgroundImage = My.Resources.Resources.Arrow0102
            Case 1
                oThread = New Thread(AddressOf AniThread2)
                oThread.Start()
                flgPage = 0
                PicArrow.BackgroundImage = My.Resources.Resources.Arrow0202
            Case Else

        End Select
    End Sub

    '此动画方案暂不稳定
    'Private Sub AniThread1()
    '    'Throw New NotImplementedException()
    '    Dim i As Integer
    '    PicCC1.BackColor = Color.White
    '    PicRA1.BackColor = Color.White
    '    PicCC2.BackColor = Color.White
    '    PicRenegade.BackColor = Color.White
    '    PicRA2.BackColor = Color.White
    '    PicGenerals.BackColor = Color.White
    '    PicCC3.BackColor = Color.White
    '    PicRA3.BackColor = Color.White
    '    PicCC4.BackColor = Color.White
    '    For i = 0 To 6
    '        Select Case (i)
    '            Case 0
    '                PicCC1.Left = PicCC1.Left + 100
    '                PicRA1.Left = PicRA1.Left + 80
    '                PicCC2.Left = PicCC2.Left + 60
    '                PicRenegade.Left = PicRenegade.Left + 40
    '                PicRA2.Left = PicRA2.Left + 20
    '            Case Else
    '                PicCC1.Left = PicCC1.Left + 100
    '                PicRA1.Left = PicRA1.Left + 100
    '                PicCC2.Left = PicCC2.Left + 100
    '                PicRenegade.Left = PicRenegade.Left + 100
    '                PicRA2.Left = PicRA2.Left + 100
    '                PicGenerals.Left = PicGenerals.Left + 100
    '        End Select
    '    Next i
    '    PicCC3.Left = 520
    '    PicCC3.Visible = True
    '    PicRA3.Left = 520
    '    PicRA3.Visible = True
    '    PicCC4.Left = 520
    '    PicCC4.Visible = True
    '    For i = 0 To 6
    '        Select Case (i)
    '            Case 0
    '                PicCC3.Left = PicCC3.Left - 100
    '                PicRA3.Left = PicRA3.Left - 120
    '            Case Else
    '                If (PicCC3.Left - 100 <= 0) Then
    '                    PicCC3.Left = 0
    '                Else
    '                    PicCC3.Left = PicCC3.Left - 100
    '                End If
    '                If (PicRA3.Left - 100 <= 0) Then
    '                    PicRA3.Left = 0
    '                Else
    '                    PicRA3.Left = PicRA3.Left - 100
    '                End If
    '                If (PicCC4.Left - 100 <= 0) Then
    '                    PicCC4.Left = 0
    '                Else
    '                    PicCC4.Left = PicCC4.Left - 100
    '                End If
    '        End Select
    '    Next i
    '    PicCC1.BackColor = Color.Transparent
    '    PicRA1.BackColor = Color.Transparent
    '    PicCC2.BackColor = Color.Transparent
    '    PicRenegade.BackColor = Color.Transparent
    '    PicRA2.BackColor = Color.Transparent
    '    PicGenerals.BackColor = Color.Transparent
    '    PicCC3.BackColor = Color.Transparent
    '    PicRA3.BackColor = Color.Transparent
    '    PicCC4.BackColor = Color.Transparent
    'End Sub

    Private Sub AniThread1()
        PicCC1.Visible = False
        PicRA1.Visible = False
        PicCC2.Visible = False
        PicRenegade.Visible = False
        PicRA2.Visible = False
        PicGenerals.Visible = False
        PicCC3.Visible = True
        PicRA3.Visible = True
        PicCC4.Visible = True
    End Sub

    Private Sub AniThread2()
        PicCC3.Visible = False
        PicRA3.Visible = False
        PicCC4.Visible = False
        PicCC1.Visible = True
        PicRA1.Visible = True
        PicCC2.Visible = True
        PicRenegade.Visible = True
        PicRA2.Visible = True
        PicGenerals.Visible = True
    End Sub

    Private Sub PicArrow_MouseMove(sender As Object, e As MouseEventArgs) Handles PicArrow.MouseMove
        If (flgLock1 = False) Then
            flgLock1 = True
            If (flgPage = 0) Then
                PicArrow.BackgroundImage = My.Resources.Resources.Arrow0202
            ElseIf (flgPage = 1) Then
                PicArrow.BackgroundImage = My.Resources.Resources.Arrow0102
            End If
        End If
    End Sub

    Private Sub PicArrow_MouseLeave(sender As Object, e As EventArgs) Handles PicArrow.MouseLeave
        flgLock1 = False
        If (flgPage = 0) Then
            PicArrow.BackgroundImage = My.Resources.Resources.Arrow0201
        ElseIf (flgPage = 1) Then
            PicArrow.BackgroundImage = My.Resources.Resources.Arrow0101
        End If
    End Sub
End Class
