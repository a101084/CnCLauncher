﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.LblSetting = New System.Windows.Forms.Label()
        Me.LblExit = New System.Windows.Forms.Label()
        Me.PicCC1 = New System.Windows.Forms.PictureBox()
        Me.PicRA1 = New System.Windows.Forms.PictureBox()
        Me.PicCC2 = New System.Windows.Forms.PictureBox()
        Me.PicRenegade = New System.Windows.Forms.PictureBox()
        Me.PicRA2 = New System.Windows.Forms.PictureBox()
        Me.PicLogo = New System.Windows.Forms.PictureBox()
        Me.PalList = New System.Windows.Forms.Panel()
        Me.PicGenerals = New System.Windows.Forms.PictureBox()
        Me.PicCC3 = New System.Windows.Forms.PictureBox()
        Me.PicRA3 = New System.Windows.Forms.PictureBox()
        Me.PicCC4 = New System.Windows.Forms.PictureBox()
        Me.PicArrow = New System.Windows.Forms.PictureBox()
        CType(Me.PicCC1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicRA1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicCC2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicRenegade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicRA2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PalList.SuspendLayout()
        CType(Me.PicGenerals, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicCC3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicRA3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicCC4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicArrow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblSetting
        '
        Me.LblSetting.BackColor = System.Drawing.Color.Transparent
        Me.LblSetting.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblSetting.Font = New System.Drawing.Font("微软雅黑", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.LblSetting.ForeColor = System.Drawing.Color.White
        Me.LblSetting.Location = New System.Drawing.Point(668, 515)
        Me.LblSetting.Name = "LblSetting"
        Me.LblSetting.Size = New System.Drawing.Size(120, 30)
        Me.LblSetting.TabIndex = 0
        Me.LblSetting.Text = "启动器设置"
        Me.LblSetting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblExit
        '
        Me.LblExit.BackColor = System.Drawing.Color.Transparent
        Me.LblExit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblExit.Font = New System.Drawing.Font("微软雅黑", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.LblExit.ForeColor = System.Drawing.Color.White
        Me.LblExit.Location = New System.Drawing.Point(668, 556)
        Me.LblExit.Name = "LblExit"
        Me.LblExit.Size = New System.Drawing.Size(120, 30)
        Me.LblExit.TabIndex = 1
        Me.LblExit.Text = "退出"
        Me.LblExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PicCC1
        '
        Me.PicCC1.BackColor = System.Drawing.Color.Transparent
        Me.PicCC1.Location = New System.Drawing.Point(0, 0)
        Me.PicCC1.Name = "PicCC1"
        Me.PicCC1.Size = New System.Drawing.Size(515, 60)
        Me.PicCC1.TabIndex = 2
        Me.PicCC1.TabStop = False
        '
        'PicRA1
        '
        Me.PicRA1.BackColor = System.Drawing.Color.Transparent
        Me.PicRA1.Location = New System.Drawing.Point(0, 66)
        Me.PicRA1.Name = "PicRA1"
        Me.PicRA1.Size = New System.Drawing.Size(515, 60)
        Me.PicRA1.TabIndex = 3
        Me.PicRA1.TabStop = False
        '
        'PicCC2
        '
        Me.PicCC2.BackColor = System.Drawing.Color.Transparent
        Me.PicCC2.Location = New System.Drawing.Point(0, 132)
        Me.PicCC2.Name = "PicCC2"
        Me.PicCC2.Size = New System.Drawing.Size(515, 60)
        Me.PicCC2.TabIndex = 4
        Me.PicCC2.TabStop = False
        '
        'PicRenegade
        '
        Me.PicRenegade.BackColor = System.Drawing.Color.Transparent
        Me.PicRenegade.Location = New System.Drawing.Point(0, 198)
        Me.PicRenegade.Name = "PicRenegade"
        Me.PicRenegade.Size = New System.Drawing.Size(515, 60)
        Me.PicRenegade.TabIndex = 5
        Me.PicRenegade.TabStop = False
        '
        'PicRA2
        '
        Me.PicRA2.BackColor = System.Drawing.Color.Transparent
        Me.PicRA2.Location = New System.Drawing.Point(0, 264)
        Me.PicRA2.Name = "PicRA2"
        Me.PicRA2.Size = New System.Drawing.Size(515, 60)
        Me.PicRA2.TabIndex = 6
        Me.PicRA2.TabStop = False
        '
        'PicLogo
        '
        Me.PicLogo.BackColor = System.Drawing.Color.Transparent
        Me.PicLogo.BackgroundImage = Global.CnCLauncher.My.Resources.Resources.bglogo
        Me.PicLogo.Location = New System.Drawing.Point(222, 10)
        Me.PicLogo.Name = "PicLogo"
        Me.PicLogo.Size = New System.Drawing.Size(356, 135)
        Me.PicLogo.TabIndex = 8
        Me.PicLogo.TabStop = False
        '
        'PalList
        '
        Me.PalList.BackColor = System.Drawing.Color.Transparent
        Me.PalList.Controls.Add(Me.PicGenerals)
        Me.PalList.Controls.Add(Me.PicRenegade)
        Me.PalList.Controls.Add(Me.PicRA2)
        Me.PalList.Controls.Add(Me.PicCC2)
        Me.PalList.Controls.Add(Me.PicCC1)
        Me.PalList.Controls.Add(Me.PicRA1)
        Me.PalList.Controls.Add(Me.PicCC3)
        Me.PalList.Controls.Add(Me.PicRA3)
        Me.PalList.Controls.Add(Me.PicCC4)
        Me.PalList.Location = New System.Drawing.Point(12, 155)
        Me.PalList.Name = "PalList"
        Me.PalList.Size = New System.Drawing.Size(515, 390)
        Me.PalList.TabIndex = 9
        '
        'PicGenerals
        '
        Me.PicGenerals.BackColor = System.Drawing.Color.Transparent
        Me.PicGenerals.Location = New System.Drawing.Point(0, 330)
        Me.PicGenerals.Name = "PicGenerals"
        Me.PicGenerals.Size = New System.Drawing.Size(515, 60)
        Me.PicGenerals.TabIndex = 10
        Me.PicGenerals.TabStop = False
        '
        'PicCC3
        '
        Me.PicCC3.BackColor = System.Drawing.Color.Transparent
        Me.PicCC3.Location = New System.Drawing.Point(0, 0)
        Me.PicCC3.Name = "PicCC3"
        Me.PicCC3.Size = New System.Drawing.Size(515, 60)
        Me.PicCC3.TabIndex = 11
        Me.PicCC3.TabStop = False
        '
        'PicRA3
        '
        Me.PicRA3.BackColor = System.Drawing.Color.Transparent
        Me.PicRA3.Location = New System.Drawing.Point(0, 66)
        Me.PicRA3.Name = "PicRA3"
        Me.PicRA3.Size = New System.Drawing.Size(515, 60)
        Me.PicRA3.TabIndex = 12
        Me.PicRA3.TabStop = False
        '
        'PicCC4
        '
        Me.PicCC4.BackColor = System.Drawing.Color.Transparent
        Me.PicCC4.Location = New System.Drawing.Point(0, 132)
        Me.PicCC4.Name = "PicCC4"
        Me.PicCC4.Size = New System.Drawing.Size(515, 60)
        Me.PicCC4.TabIndex = 13
        Me.PicCC4.TabStop = False
        '
        'PicArrow
        '
        Me.PicArrow.BackColor = System.Drawing.Color.Transparent
        Me.PicArrow.BackgroundImage = Global.CnCLauncher.My.Resources.Resources.Arrow0201
        Me.PicArrow.Location = New System.Drawing.Point(220, 556)
        Me.PicArrow.Name = "PicArrow"
        Me.PicArrow.Size = New System.Drawing.Size(100, 25)
        Me.PicArrow.TabIndex = 10
        Me.PicArrow.TabStop = False
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.CnCLauncher.My.Resources.Resources.bg
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(800, 606)
        Me.Controls.Add(Me.PicArrow)
        Me.Controls.Add(Me.PicLogo)
        Me.Controls.Add(Me.LblExit)
        Me.Controls.Add(Me.LblSetting)
        Me.Controls.Add(Me.PalList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "CnCLauncher"
        CType(Me.PicCC1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicRA1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicCC2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicRenegade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicRA2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PalList.ResumeLayout(False)
        CType(Me.PicGenerals, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicCC3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicRA3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicCC4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicArrow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LblSetting As Windows.Forms.Label
    Friend WithEvents LblExit As Windows.Forms.Label
    Friend WithEvents PicCC1 As PictureBox
    Friend WithEvents PicRA1 As PictureBox
    Friend WithEvents PicCC2 As PictureBox
    Friend WithEvents PicRenegade As PictureBox
    Friend WithEvents PicRA2 As PictureBox
    Friend WithEvents PicLogo As PictureBox
    Friend WithEvents PalList As Panel
    Friend WithEvents PicGenerals As PictureBox
    Friend WithEvents PicArrow As PictureBox
    Friend WithEvents PicCC3 As PictureBox
    Friend WithEvents PicRA3 As PictureBox
    Friend WithEvents PicCC4 As PictureBox
End Class
