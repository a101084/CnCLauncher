﻿Imports System.Threading

Public Class frmSetting
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim CheckThread = New Thread(AddressOf CheckUpdate)
        CheckThread.Start()
    End Sub

    Private Sub CheckUpdate()
        'Dim strVerLog As String
        'Dim strTmp() As String
        'Dim Web As New Net.WebClient
        'Dim strUri As New Uri(strLogURI)
        'Try
        '    strVerLog = Web.DownloadString(strUri)
        'Catch ex As Exception
        '    strVerLog = "0,5pqC5peg5pu05paw5pel5b+X"
        'End Try
        'strTmp = Split(strVerLog, ",")
        'Dim strLog As String
        'strLog = System.Text.Encoding.GetEncoding("utf-8").GetString(Convert.FromBase64String("5pqC5peg5pu05paw5pel5b+X"))
        'If (CInt(strTmp(0)) > intVer) Then
        '    MessageBox.Show(Me, "发现新的版本，请到下载页面手动更新。" + vbCrLf + vbCrLf + "更新日志：" + vbCrLf + strLog, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Process.Start(strUpdateURI)
        'Else
        '    MessageBox.Show(Me, "当前已是最新版本，无需更新。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Process.Start("https://gitee.com/a101084/CnCLauncher/")
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        OpenFileDialog.Filter = "图像文件|*.jpg;*.bmp;*.bmp;*.gif;*.gif;*.png;*.png|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox1.Text = OpenFileDialog.FileName
            WriteINI("Setting", "BG", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        OpenFileDialog.Filter = "光标文件|*.cur;*.ani;*.ico|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox2.Text = OpenFileDialog.FileName
            WriteINI("Setting", "CUR", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox3.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "CC1", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox4.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "RA1", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox5.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "CC2", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox6.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "Renegade", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox7.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "RA2", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox8.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "RA2YURI", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox9.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "Generals", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox10.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "GeneralsZH", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox11.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "CC3", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox12.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "CC3KW", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox13.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "RA3", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox14.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "RA3Uprising", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        OpenFileDialog.Filter = "应用程序|*.exe|所有文件|*.*"
        OpenFileDialog.ShowDialog()
        If (OpenFileDialog.FileName <> "") Then
            TextBox15.Text = OpenFileDialog.FileName
            WriteINI("CnCLauncher", "CC4", OpenFileDialog.FileName, strConfig)
        End If
    End Sub

    Private Sub frmSetting_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Text = GetINI("Setting", "BG", "", strConfig)
        TextBox2.Text = GetINI("Setting", "CUR", "", strConfig)

        TextBox3.Text = GetINI("CnCLauncher", "CC1", "", strConfig)
        TextBox4.Text = GetINI("CnCLauncher", "RA1", "", strConfig)
        TextBox5.Text = GetINI("CnCLauncher", "CC2", "", strConfig)
        TextBox6.Text = GetINI("CnCLauncher", "Renegade", "", strConfig)
        TextBox7.Text = GetINI("CnCLauncher", "RA2", "", strConfig)
        TextBox8.Text = GetINI("CnCLauncher", "RA2YURI", "", strConfig)
        TextBox9.Text = GetINI("CnCLauncher", "Generals", "", strConfig)
        TextBox10.Text = GetINI("CnCLauncher", "GeneralsZH", "", strConfig)
        TextBox11.Text = GetINI("CnCLauncher", "CC3", "", strConfig)
        TextBox12.Text = GetINI("CnCLauncher", "CC3KW", "", strConfig)
        TextBox13.Text = GetINI("CnCLauncher", "RA3", "", strConfig)
        TextBox14.Text = GetINI("CnCLauncher", "RA3Uprising", "", strConfig)
        TextBox15.Text = GetINI("CnCLauncher", "CC4", "", strConfig)
    End Sub
End Class
