@Echo off

cd /d %~dp0
"%WINDIR%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe" CnCLauncher.sln /t:Rebuild /p:Configuration=Debug /p:Platform="Any CPU"
del ..\NewCnCLauncher.zip
Resources\7z.exe a -tzip -y ..\NewCnCLauncher.zip .\bin\Debug\CnCLauncher.exe
pause
start ..\Tools\LogTool\bin\Debug\LogTool.exe
