﻿Public Class frmMain
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim strText() As String
        strText = Split(My.Computer.FileSystem.ReadAllText("..\..\..\..\VerLog.txt", System.Text.Encoding.ASCII), ",")
        TextBox1.Text = NewDate(strText(0))
        TextBox3.Text = strText(0)
        TextBox2.Text = System.Text.Encoding.GetEncoding("utf-8").GetString(Convert.FromBase64String(strText(1)))
        TextBox5.Text = strText(2)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim strText As String
        If (TextBox3.Text = "" Or TextBox2.Text = "" Or TextBox3.Text = "") Then
            MessageBox.Show("时间戳、更新日志、更新包数据摘要必须填写完整。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            strText = TextBox3.Text & "," & Convert.ToBase64String(System.Text.Encoding.GetEncoding("utf-8").GetBytes(TextBox2.Text)) & "," & TextBox5.Text
            My.Computer.FileSystem.WriteAllText("..\..\..\..\VerLog.txt", strText, False)
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        TextBox1.Text = Now.ToString
        TextBox3.Text = GetTimestamp(TextBox1.Text)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        TextBox5.Text = UCase(GetMD5("..\..\..\..\NewCnCLauncher.zip"))
    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If (e.KeyCode = Keys.Enter) Then
            TextBox3.Text = GetTimestamp(TextBox1.Text)
        End If
    End Sub

    Private Sub TextBox3_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox3.KeyDown
        If (e.KeyCode = Keys.Enter) Then
            TextBox1.Text = NewDate(TextBox3.Text)
        End If
    End Sub

    Public Function GetTimestamp(Times As DateTime) As Long
        Return ((Times.Ticks - TimeZone.CurrentTimeZone.ToLocalTime(New DateTime(1970, 1, 1, 0, 0, 0, 0)).Ticks) / 10000000)
    End Function

    Public Function NewDate(timestamp As Long) As DateTime
        Dim tt As Long
        tt = TimeZone.CurrentTimeZone.ToLocalTime(New DateTime(1970, 1, 1, 0, 0, 0, 0)).Ticks + timestamp * 10000000
        Return (New DateTime(tt))
    End Function

    Public Function GetMD5(ByVal strSource As String) As String
        Dim result As String = ""
        Try
            Dim fstream As New IO.FileStream(strSource, IO.FileMode.Open, IO.FileAccess.Read)
            Dim dataToHash(fstream.Length - 1) As Byte
            fstream.Read(dataToHash, 0, fstream.Length)
            fstream.Close()
            Dim hashvalue As Byte() = CType(Security.Cryptography.CryptoConfig.CreateFromName("MD5"), Security.Cryptography.HashAlgorithm).ComputeHash(dataToHash)
            Dim i As Integer
            For i = 0 To hashvalue.Length - 1
                result += Microsoft.VisualBasic.Right("00" + Hex(hashvalue(i)).ToLower, 2)
            Next
            Return result
        Catch ex As Exception
            'MsgBox(ex.Message)
            Return result
        End Try
    End Function

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If (IO.File.Exists("..\..\..\..\NewCnCLauncher.zip") = False) Then
            MessageBox.Show("更新包不存在。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        End If
    End Sub
End Class
