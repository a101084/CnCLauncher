# CnCLauncher

#### 介绍

命令与征服十年版启动器的重制项目，支持最新版Windows操作系统，并且增加更多自定义功能，以及漂亮可定制的界面。

#### 软件截图

![](https://gitee.com/a101084/CnCLauncher/raw/master/Screenshots/20200330.png)

![](https://gitee.com/a101084/CnCLauncher/raw/master/Screenshots/20200330.gif)

#### 使用说明

程序正常运行需要安装.Net 4.0以上版本框架。

本仓库目录结构

```
Old						#存放老版本程序
QRCode					#用来打赏作者的二维码
Screenshots				#软件截图
Src						#源码
Tools					#一些用到的工具
.gitignore				#Git忽略列表
LICENSE					#GPL开源许可协议
NewCnCLauncher.zip		#最新版主程序
README.md				#本说明文件
VerLog.txt				#更新日志
```

请选择自己需要的版本下载，如果无从选择的话建议点此处下载**[最新版本](https://gitee.com/a101084/CnCLauncher/raw/master/NewCnCLauncher.zip)**。

下载完成直接解压（7Zip、Bandizip等）替换原文件即可，或者放到任何你喜欢的位置。

#### 更新日志

1.添加“更新日志生成器”工具

2.优化更新模块，支持自动下载更新

3.修正部分已知的小问题

#### 更新计划

~~1.新的自动更新功能~~

~~2.新的程序设置界面~~

3.游戏列表的翻页动画

4.新的游戏列表显示方式

5.游戏列表项目自定义

6.自动通过注册表获取游戏位置

7.自动添加游戏相关注册表

8.程序背景支持动态画面

9.支持Mod切换

更多计划待完善...

#### 软件声明

作者只能保证启动程序本身没有任何危害用户的代码，但作者无法保证随启动器附带的游戏是安全的，还请用户自行分辨。

本软件完全免费，欢迎打赏捐助，您的支持是我们最大的动力。

#### 意见反馈

QQ：1010841065

MAIL：1010841065@QQ.COM

#### 捐助

喜欢这个项目？请作者喝杯咖啡，或者与作者分享你喜欢的零食。

提示：捐助是自愿的，表示对本软件的支持，并没有提供额外的功能。

![](https://gitee.com/a101084/CnCLauncher/raw/master/QRCode/zhifubao.png)
![](https://gitee.com/a101084/CnCLauncher/raw/master/QRCode/qq.png)
![](https://gitee.com/a101084/CnCLauncher/raw/master/QRCode/weixin.png)
